from templates.config import PAGES


class TopMenu:
    menu = ''

    def __init__(self):
        for page, page_title in PAGES.items():
            self.menu += '<a href="/%s">%s</a>&nbsp;&nbsp;&nbsp;' % (page, page_title)

    def generate_menu(self):
        return self.menu
