from templates.top_menu import TopMenu

from templates.config import PAGES


class GeneralTemplate:
    def __init__(self, title):
        self.t = title

    def create_body(self):
        return '<p><b>This is the %s page!</p>' % PAGES[self.t]

    def create_page(self):
        top_menu = TopMenu().generate_menu()
        body = self.create_body()
        return top_menu + body
