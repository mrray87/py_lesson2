from bottle import (
    get,
    run
)

from templates.general import GeneralTemplate


@get('/<page_name>')
def page(page_name='index'):
    return GeneralTemplate(page_name).create_page()


@get('/')
def index():
    return GeneralTemplate('index').create_page()


if __name__ == "__main__":
    run(host='localhost', port=8080)
